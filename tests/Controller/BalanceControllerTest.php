<?php


namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BalanceControllerTest extends WebTestCase
{
    public function testGetBalance()
    {
        $client = static::createClient();

        $client->request('GET', '/balance?account_id=100');

        $this->assertStringContainsString('20', $client->getResponse()->getContent());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testGetBalanceNotFound()
    {
        $client = static::createClient();

        $client->request('GET', '/balance?account_id=1234');

        $this->assertStringContainsString('0', $client->getResponse()->getContent());
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }
}