<?php


namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EventControllerTest extends WebTestCase
{
    public function testCreateAccount()
    {
        $client = static::createClient();

        $event = [];
        $event['type'] = "deposit";
        $event['destination'] = "100";
        $event['amount'] = 10;

        $client->request('POST', '/event', [], [], [], json_encode($event));

        $this->assertJsonStringEqualsJsonString('{"destination": {"id":"100", "balance":10}}', $client->getResponse()->getContent());
        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    public function testDepositAccount()
    {
        $client = static::createClient();

        $event = [];
        $event['type'] = "deposit";
        $event['destination'] = "100";
        $event['amount'] = 10;

        $client->request('POST', '/event', [], [], [], json_encode($event));

        $this->assertJsonStringEqualsJsonString('{"destination": {"id":"100", "balance":20}}', $client->getResponse()->getContent());
        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    public function testWithdraw()
    {
        $client = static::createClient();

        $event = [];
        $event['type'] = "withdraw";
        $event['origin'] = "100";
        $event['amount'] = 5;

        $client->request('POST', '/event', [], [], [], json_encode($event));

        $this->assertJsonStringEqualsJsonString('{"origin": {"id":"100", "balance":15}}', $client->getResponse()->getContent());
        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    public function testWithdrawNotFound()
    {
        $client = static::createClient();

        $event = [];
        $event['type'] = "withdraw";
        $event['origin'] = "200";
        $event['amount'] = 5;

        $client->request('POST', '/event', [], [], [], json_encode($event));

        $this->assertStringContainsString('0', $client->getResponse()->getContent());
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    public function testTransfer()
    {
        $client = static::createClient();

        $event = [];
        $event['type'] = "transfer";
        $event['origin'] = "100";
        $event['destination'] = "300";
        $event['amount'] = 15;

        $client->request('POST', '/event', [], [], [], json_encode($event));

        $this->assertJsonStringEqualsJsonString(
            '{"origin": {"id":"100", "balance":0}, "destination": {"id":"300", "balance":15}}',
            $client->getResponse()->getContent()
        );
        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }
}