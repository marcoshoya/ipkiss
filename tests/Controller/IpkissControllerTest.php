<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class IpkissControllerTest extends WebTestCase
{
    public function testReset()
    {
        $client = static::createClient();

        $client->request('POST', '/reset');

        $this->assertStringContainsString('OK', $client->getResponse()->getContent());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testResetFail()
    {
        $this->expectException(MethodNotAllowedHttpException::class);

        $client = static::createClient();
        $client->catchExceptions(false);

        $client->request('GET', '/reset');
    }

}