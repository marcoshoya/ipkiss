<?php


namespace App\Tests\Service;

use App\Service\BalanceService;
use PHPUnit\Framework\TestCase;

class BalanceServiceTest extends TestCase
{
    public function testGetBalance()
    {
        $service = new BalanceService();

        $balance = $service->getBalance(100);
        $balanceNotFound = $service->getBalance(1234);

        $this->assertEquals(20, $balance->getBalance());
        $this->assertNull($balanceNotFound);

    }

}