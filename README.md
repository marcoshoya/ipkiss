Ipkiss Tester
=============

Small app created in PHP using Symfony 5 framework based on [Ipkiss Tester](https://ipkiss.pragmazero.com/)

Note: because PHP is stateless request, it was used sqlite db in order to persist data for create account operation.


Requirements
------------
 - PHP 7.2
 - Symfony 5.1
 - Composer

Install
-------
Clone app from https://bitbucket.org/marcoshoya/ipkiss

    git clone https://marcoshoya@bitbucket.org/marcoshoya/ipkiss.git

Then browse to the directory folder and run composer

    composer install
    
RUnning the app
---------------
To run in local webserver

    symfony server:start
    
Then browse to http://localhost:8000/

Deploying
---------
In case of running in local symfony server

    ngrok http 8000

Running the Tests
-----------------

Install the [Composer](http://getcomposer.org/) `dev` dependencies:

    php composer.phar install --dev

Then, run the test suite using
[PHPUnit](https://github.com/sebastianbergmann/phpunit/):

    ./bin/phpunit
    
Virtual host configuration
--------------------------

```js
<VirtualHost :80>
   
    DocumentRoot "C:/path/to/root/my_project/public"
    ServerName ipkiss.localhost.com
	
	DirectoryIndex index.php

    <Directory "C:/path/to/root/my_project/public">
        Options Indexes FollowSymLinks Includes ExecCGI
	    AllowOverride none
	    Require all granted 
		
	    <IfModule mod_rewrite.c>
            Options -MultiViews
            RewriteEngine On
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteRule ^(.*)$ index.php [QSA,L]
            RewriteCond %{HTTP:Authorization} ^(.*)
            RewriteRule .* - [e=HTTP_AUTHORIZATION:%1]
        </IfModule>
		
    </Directory>
   
</VirtualHost>
```