<?php


namespace App\Repository;

use App\Entity\Account;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AccountRepository  extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Account::class);
    }

    public function save(Account $account) : void
    {
        $this->_em->persist($account);
        $this->_em->flush();
    }

    public function reset() : void
    {
        // truncate
        $query = $this->_em->createQuery("DELETE FROM App\Entity\Account a");
        $query->execute();

        $account = new Account(300, 0);
        $this->save($account);
    }
}