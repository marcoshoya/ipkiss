<?php


namespace App\Service;


use App\Entity\Balance;

class BalanceService
{
    private $acc = [];

    public function __construct()
    {
        // @todo: replace this to use collection of data from db
        $balance = new Balance(100, 20);
        array_push($this->acc, $balance);
    }

    /**
     * @param int $id
     * @return Balance
     */
    public function getBalance($id)
    {
        foreach ($this->acc as $acc) {
            if ($acc->getAccountId() == $id) {
                return $acc;
            }
        }

        return null;
    }

}