<?php


namespace App\Service;

use App\Entity\Account;
use App\Entity\Event;
use App\Model\DepositResponse;
use App\Model\TransferResponse;
use App\Model\WithdrawResponse;
use App\Repository\AccountRepository;

class EventService
{
    protected $repository;

    public function __construct(AccountRepository $repository)
    {
        $this->repository = $repository;
    }

    public function reset() : void
    {
        $this->repository->reset();
    }

    /**
     * createDeposit function
     *
     * @param Event $event
     * @return DepositResponse|null
     */
    public function createDeposit(Event $event): ?DepositResponse
    {
        $destination = $this->fetchAccount($event->getDestination());
        if (null === $destination) {

            $destination = new Account($event->getDestination(), 0);
        }

        $destination->deposit($event->getAmount());

        $this->repository->save($destination);

        return new DepositResponse($destination);
    }

    /**
     * createWithdraw function
     *
     * @param Event $event
     * @return WithdrawResponse|null
     */
    public function createWithdraw(Event $event) : ?WithdrawResponse
    {
        $origin = $this->fetchAccount($event->getOrigin());
        if (null === $origin) {
            return null;
        }

        $origin->withdraw($event->getAmount());
        $this->repository->save($origin);

        return new WithdrawResponse($origin);
    }

    /**
     * @param Event $event
     *
     * @return TransferResponse|null
     */
    public function createTransfer(Event $event) : ?TransferResponse
    {
        $origin = $this->fetchAccount($event->getOrigin());
        if (null === $origin) {
            return null;
        }

        $destination = $this->fetchAccount($event->getDestination());
        if (null === $destination) {
            return null;
        }

        $origin->withdraw($event->getAmount());
        $destination->deposit($event->getAmount());

        $this->repository->save($origin);
        $this->repository->save($destination);

        return new TransferResponse($origin, $destination);
    }

    private function fetchAccount($id) : ?Account
    {
        $account = $this->repository->find($id);

        return $account ? $account : null;
    }

}