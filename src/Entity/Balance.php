<?php


namespace App\Entity;


class Balance
{

    /**
     * @var integer
     */
    private $account_id;

    /**
     * @var integer
     */
    private $balance;

    /**
     * Balance constructor.
     * @param int $account_id
     * @param int $balance
     */
    public function __construct(int $account_id, int $balance)
    {
        $this->account_id = $account_id;
        $this->balance = $balance;
    }

    /**
     * @return int
     */
    public function getAccountId(): int
    {
        return $this->account_id;
    }

    /**
     * @param int $account_id
     */
    public function setAccountId(int $account_id): void
    {
        $this->account_id = $account_id;
    }

    /**
     * @return int
     */
    public function getBalance(): int
    {
        return $this->balance;
    }

    /**
     * @param int $balance
     */
    public function setBalance(int $balance): void
    {
        $this->balance = $balance;
    }


}