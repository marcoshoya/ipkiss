<?php


namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Event
{
    const DEPOSIT = "deposit";
    const WITHDRAW = "withdraw";
    const TRANSFER = "transfer";

    const TYPES = [self::DEPOSIT, self::WITHDRAW, self::TRANSFER];

    /**
     * @var string
     *
     * @Assert\Choice(
     *     choices=Event::TYPES,
     *     message = "Choose a valid type"
 *     )
     */
    private $type;

    /**
     * @var string
     *
     * @Assert\Type("string")
     */
    private $origin = "";

    /**
     * @var string
     *
     * @Assert\Type("string")
     */
    private $destination = "";

    /**
     * @var integer
     *
     * @Assert\NotNull(message="Missing field: amount")
     */
    private $amount;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getOrigin(): string
    {
        return $this->origin;
    }

    /**
     * @param string $origin
     */
    public function setOrigin(string $origin): void
    {
        $this->origin = $origin;
    }

    /**
     * @return string
     */
    public function getDestination(): string
    {
        return $this->destination;
    }

    /**
     * @param string $destination
     */
    public function setDestination(string $destination): void
    {
        $this->destination = $destination;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount(int $amount): void
    {
        $this->amount = $amount;
    }


}