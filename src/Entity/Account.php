<?php


namespace App\Entity;

use App\Repository\AccountRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AccountRepository::class)
 */
class Account
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $balance;

    /**
     * Account constructor.
     * @param string $id
     * @param int $balance
     */
    public function __construct(string $id, int $balance)
    {
        $this->id = $id;
        $this->balance = $balance;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getBalance(): int
    {
        return $this->balance;
    }

    /**
     * @param int $balance
     */
    public function setBalance(int $balance): void
    {
        $this->balance = $balance;
    }

    public function deposit($amount): void
    {
        $new = bcadd($this->getBalance(), $amount);
        $this->setBalance($new);
    }

    public function withdraw($amount): void
    {
        $new = bcsub($this->getBalance(), $amount);
        $this->setBalance($new);
    }


}