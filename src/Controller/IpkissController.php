<?php

namespace App\Controller;

use App\Service\EventService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class IpkissController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"})
     */
    public function index() : Response
    {
        return new Response(null, Response::HTTP_OK);
    }

    /**
     * @Route("/reset", methods={"POST"})
     */
    public function reset(EventService $service) : Response
    {
        $service->reset();
        return new Response("OK", Response::HTTP_OK);
    }

}