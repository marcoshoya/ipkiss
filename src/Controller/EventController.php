<?php


namespace App\Controller;

use App\Entity\Event;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Service\EventService;

class EventController extends AbstractController
{

    /**
     * @Route("/event", methods={"POST"})
     *
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param ValidatorInterface $validator
     * @param EventService $service
     *
     * @return Response
     */
    public function createAccount(Request $request, SerializerInterface $serializer, ValidatorInterface $validator, EventService $service) : Response
    {

        $event = $serializer->deserialize($request->getContent(), Event::class, 'json');
        $errors = $validator->validate($event);

        if (count($errors)) {
            $errorList['errors'] = [];
            foreach ($errors as $error) {
                $errorList['errors'][]['error'] = $error->getMessage();
            }

            return $this->json($errorList, Response::HTTP_BAD_REQUEST);

        } else {

            $response = null;
            switch ($event->getType()) {
                case Event::DEPOSIT:
                    $response = $service->createDeposit($event);
                    break;
                case Event::WITHDRAW:
                    $response = $service->createWithdraw($event);
                    break;
                case Event::TRANSFER:
                    $response = $service->createTransfer($event);
                    break;
            }

            if (null !== $response) {

                return $this->json($response, Response::HTTP_CREATED);
            } else {

                return new Response(0, Response::HTTP_NOT_FOUND);
            }


        }

    }
}