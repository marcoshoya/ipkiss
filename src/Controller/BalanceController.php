<?php


namespace App\Controller;


use App\Entity\Balance;
use App\Service\BalanceService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BalanceController extends AbstractController
{
    /**
     * @Route("/balance", methods={"GET"})
     *
     * @param Request $request
     * @param BalanceService $service
     * @return Response
     */
    public function index(Request $request, BalanceService $service) : Response
    {
        if (!$request->query->has('account_id')) {
            throw new InvalidArgumentException();
        }

        $id = $request->query->get('account_id');
        $balance = $service->getBalance($id);

        if ($balance instanceof Balance) {
            return new Response($balance->getBalance(), Response::HTTP_OK);
        } else {
            return new Response(0, Response::HTTP_NOT_FOUND);
        }
    }

}