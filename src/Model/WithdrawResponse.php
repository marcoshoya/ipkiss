<?php


namespace App\Model;

use App\Entity\Account;

class WithdrawResponse
{
    /**
     * @var Account
     */
    private $origin;

    /**
     * WithdrawResponse constructor.
     * @param Account $origin
     */
    public function __construct(Account $origin)
    {
        $this->origin = $origin;
    }


    /**
     * @return Account
     */
    public function getOrigin(): Account
    {
        return $this->origin;
    }

    /**
     * @param Account $origin
     */
    public function setOrigin(Account $origin): void
    {
        $this->origin = $origin;
    }


}