<?php


namespace App\Model;

use App\Entity\Account;

class DepositResponse
{
    /**
     * @var Account
     */
    private $destination;

    /**
     * DepositResponse constructor.
     * @param Account $destination
     */
    public function __construct(Account $destination)
    {
        $this->destination = $destination;
    }

    /**
     * @return Account
     */
    public function getDestination(): Account
    {
        return $this->destination;
    }

    /**
     * @param Account $destination
     */
    public function setDestination(Account $destination): void
    {
        $this->destination = $destination;
    }




}