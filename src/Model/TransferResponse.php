<?php


namespace App\Model;

use App\Entity\Account;


class TransferResponse
{
    /**
     * @var Account
     */
    private $origin;

    /**
     * @var Account
     */
    private $destination;

    /**
     * TransferResponse constructor.
     *
     * @param Account $origin
     * @param Account $destination
     */
    public function __construct(Account $origin, Account $destination)
    {
        $this->origin = $origin;
        $this->destination = $destination;
    }

    /**
     * @return Account
     */
    public function getOrigin(): Account
    {
        return $this->origin;
    }

    /**
     * @param Account $origin
     */
    public function setOrigin(Account $origin): void
    {
        $this->origin = $origin;
    }

    /**
     * @return Account
     */
    public function getDestination(): Account
    {
        return $this->destination;
    }

    /**
     * @param Account $destination
     */
    public function setDestination(Account $destination): void
    {
        $this->destination = $destination;
    }


}